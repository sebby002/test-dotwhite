﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Models
{
    public class Puzzle
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsComplete { get; set; }
    }
}
