﻿using Data.Models;
using Microsoft.AspNetCore.Mvc;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(IRepo repo) : base(repo)
        {
            var puzzleItems = _repo.PuzzleController.GetPuzzles().Result;

            if (!puzzleItems.Any())
            {
                for (int i = 0; i < 10; i++)
                {
                    Puzzle p = new Puzzle()
                    {
                        Name = $"Puzzle {i}",
                        IsComplete = true
                    };

                    _repo.PuzzleController.Insert(p);
                }
            }
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<object>>> Get()
        {
            return await _repo.PuzzleController.GetPuzzles();
        }


    }
}
