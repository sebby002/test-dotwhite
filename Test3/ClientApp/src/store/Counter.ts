import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface CounterState {
    count: number,
    name? : string,
    serverRequestMessage?: string;

}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.
// Use @typeName and isActionType for type detection that works even after serialization/deserialization.

export interface IncrementCountAction { type: 'INCREMENT_COUNT' }
export interface DecrementCountAction { type: 'DECREMENT_COUNT' }
export interface ChangeNameAction { type: 'CHANGE_NAME', eval: 'test' }
export interface ServerRequestAction {type: 'SERVER_REQUEST', responseMessage:string }


// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
export type KnownAction = IncrementCountAction | DecrementCountAction | ChangeNameAction |ServerRequestAction; 

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    increment: () => ({ type: 'INCREMENT_COUNT' } as IncrementCountAction),
    decrement: () => ({ type: 'DECREMENT_COUNT' } as DecrementCountAction),
    changeName: (name : string) => ({type: 'CHANGE_NAME', eval:name } as ChangeNameAction),

    //serverRequest: () => ({ type: 'SERVER_REQUEST' } as ServerRequestAction),

    serverRequest: (): AppThunkAction<ServerRequestAction> => (dispatch, getState) => {
            fetch("weatherforecast/getmessage")
                .then(response => response.json() as Promise<string>)
                .then(data => {
                    dispatch({ type: 'SERVER_REQUEST', responseMessage: data });
                });
     }
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

export const reducer: Reducer<CounterState> = (state: CounterState | undefined, incomingAction: Action): CounterState => {

    if (state === undefined) {
        return { count: 0, name :""};
    }
    console.log(state.name)

    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'INCREMENT_COUNT':
            return { count: state.count + 1, name :state.name};
        case 'DECREMENT_COUNT':
            return { count: state.count - 1, name :state.name };
        case 'CHANGE_NAME':
            return { count: state.count, name :action.eval};
        case 'SERVER_REQUEST':
            return { count: state.count, serverRequestMessage : action.responseMessage }
        default:
            return state;
    }
};
