import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../store';
import * as CounterStore from '../store/Counter';


type CounterProps =
    CounterStore.CounterState &
    typeof CounterStore.actionCreators &
    RouteComponentProps<{}>;

class Counter extends React.PureComponent<CounterProps> {

    
    public render() {
        return (
            <React.Fragment>
                <h1>Counter</h1>

                <p>This is a simple example of a React component.</p>

                <p aria-live="polite">Current count: <strong>{this.props.count}</strong></p>

                {this.renderName()}


                <p aria-live="polite">Current name: <strong>{this.props.name}</strong></p>

                
                <button type="button"
                    className="btn btn-primary btn-lg"
                    onClick={() => { this.props.increment(); }}>
                    Increment
                </button>
                <button type="button"
                    className="btn btn-primary btn-lg"
                    onClick={() => { this.props.decrement(); }}>
                    decrement
                </button>
                <button type="button"
                    className="btn btn-primary btn-lg"
                    onClick={() =>this.props.changeName("")}>
                    Clear Name
                </button>
                <button type="button"
                    className="btn btn-primary btn-lg"
                    onClick={() =>this.props.serverRequest()}>
                    Request to server
                </button>
            </React.Fragment>
        );
    }

    private renderName() {
        return (
            <div>
                Name:
                <input type= "text" value={this.props.name} onChange={(event)=> this.props.changeName(event.target.value) }  name="test" ></input>
          </div>
        );
      }

    
};

export default connect(
    (state: ApplicationState) => state.counter,
    CounterStore.actionCreators
)(Counter as any);
