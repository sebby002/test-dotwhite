﻿using Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Controllers
{
    public abstract class BaseController
    {
        protected DataContext _dbContext { get; set; }
        public BaseController(DataContext dbContext)
        {
            _dbContext = dbContext;
        }


        #region caching
        // must be implemented

        #endregion


    }
}
