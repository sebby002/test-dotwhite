﻿using Data;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Controllers
{
    public class PuzzleController : BaseController, IPuzzleController
    {
        public PuzzleController(DataContext dbContext) : base(dbContext)
        {

        }

        public async Task<List<Puzzle>> GetPuzzles()
        {
            // improve, add cache and get list from cache
            return await _dbContext.Puzzles.ToListAsync();
        }

        public async Task<Puzzle> GetPuzzleById(long id)
        {
            // improve, add cache and get item from cache
            return await _dbContext.Puzzles.FindAsync(id);
        }

        public async void Insert(Puzzle puzzle)
        {
            // improve, update cache if it will be added
            _dbContext.Puzzles.Add(puzzle);
            await _dbContext.SaveChangesAsync();
        }

        public async void Update(Puzzle puzzle)
        {
            _dbContext.Puzzles.Update(puzzle);
            await _dbContext.SaveChangesAsync();
        }

        public async void Delete(Puzzle puzzle)
        {
            _dbContext.Puzzles.Remove(puzzle);
            await _dbContext.SaveChangesAsync();
        }


    }
}
