﻿using Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repository.Controllers
{
    public interface IPuzzleController
    {
        void Delete(Puzzle puzzle);
        Task<Puzzle> GetPuzzleById(long id);
        Task<List<Puzzle>> GetPuzzles();
        void Insert(Puzzle puzzle);
        void Update(Puzzle puzzle);
    }
}