﻿using Repository.Controllers;

namespace Repository
{
    public interface IRepo
    {
        IPuzzleController PuzzleController { get; }
    }
}