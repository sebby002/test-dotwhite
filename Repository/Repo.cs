﻿using Data;
using Microsoft.EntityFrameworkCore;
using Repository.Controllers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{
    public class Repo : IRepo
    {
        private DataContext _dbContext { get; set; }
        public Repo(DataContext dbContext)
        {
            _dbContext = dbContext;
        }

        private IPuzzleController _puzzleController { get; set; }
        public IPuzzleController PuzzleController
        {
            get { return _puzzleController ?? (_puzzleController = new PuzzleController(_dbContext)); }
        }
    }

}

